const arrayOfObjects = [
  {
    id: 1,
    name: "Alice",
    age: 30,
    email: "alice@example.com",
    city: "New York",
    country: "USA",
    hobbies: ["reading", "painting"],
    isStudent: false,
  },
  {
    id: 2,
    name: "Bob",
    age: 25,
    email: "bob@example.com",
    city: "London",
    country: "UK",
    hobbies: ["playing guitar", "hiking"],
    isStudent: true,
  },
  {
    id: 3,
    name: "Charlie",
    age: 35,
    email: "charlie@example.com",
    city: "Paris",
    country: "France",
    hobbies: ["cooking", "gardening"],
    isStudent: false,
  },
  {
    id: 4,
    name: "David",
    age: 28,
    email: "david@example.com",
    city: "Berlin",
    country: "Germany",
    hobbies: ["photography", "traveling"],
    isStudent: true,
  },
  {
    id: 5,
    name: "Eve",
    age: 32,
    email: "eve@example.com",
    city: "Sydney",
    country: "Australia",
    hobbies: ["yoga", "surfing"],
    isStudent: false,
  },
  {
    id: 6,
    name: "Frank",
    age: 33,
    email: "frank@example.com",
    city: "Los Angeles",
    country: "USA",
    hobbies: ["playing basketball", "reading"],
    isStudent: true,
  },
  {
    id: 7,
    name: "Grace",
    age: 29,
    email: "grace@example.com",
    city: "Toronto",
    country: "Canada",
    hobbies: ["painting", "running"],
    isStudent: false,
  },
  {
    id: 8,
    name: "Hannah",
    age: 31,
    email: "hannah@example.com",
    city: "Melbourne",
    country: "Australia",
    hobbies: ["writing", "knitting"],
    isStudent: true,
  },
  {
    id: 9,
    name: "Ivy",
    age: 27,
    email: "ivy@example.com",
    city: "Tokyo",
    country: "Japan",
    hobbies: ["playing piano", "cooking"],
    isStudent: false,
  },
  {
    id: 10,
    name: "Jack",
    age: 34,
    email: "jack@example.com",
    city: "Mumbai",
    country: "India",
    hobbies: ["playing cricket", "watching movies"],
    isStudent: true,
  },
];

// Given the dataset of individuals, write a function that accesses and returns the email addresses of all individuals.

function getEmails(users) {
  let emails = [];

  users.forEach((user) => {
    emails.push(user.email);
  });

  return emails;
}

// Implement a function that retrieves and prints the hobbies of individuals with a specific age, say 30 years old.

function getHobbiesByAge(users, age) {
  let hobbies = [];

  users.forEach((user) => {
    if (user.age == age) {
      hobbies.push(user.hobbies);
    }
  });

  return hobbies;
}

// Create a function that extracts and displays the names of individuals who are students (`isStudent: true`) and live in Australia.

function getStudentsByCountry(users, country) {
  let students = [];

  users.forEach((user) => {
    if (user.isStudent && user.country == country) {
      students.push(user.name);
    }
  });

  return students;
}

// Write a function that accesses and logs the name and city of the individual at the index position 3 in the dataset.

function getUserByIndex(users, index) {
  let userData = null;

  if (index >= 0 && index < users.length) {
    userData = {
      name: users[index].name,
      city: users[index].city,
    };
  }

  return userData;
}

// Implement a loop to access and print the ages of all individuals in the dataset.

function getAges(users) {
  let ages = [];

  users.forEach((user) => {
    ages.push(user.age);
  });

  return ages;
}

// Create a function to retrieve and display the first hobby of each individual in the dataset.

function getFirstHobbies(users) {
  let firstHobbies = [];

  users.forEach((user) => {
    firstHobbies.push(user.hobbies[0]);
  });

  return firstHobbies;
}

// Write a function that accesses and prints the names and email addresses of individuals aged 25.

function getNamesAndEmailsByAge(users, age) {
  let data = [];

  users.forEach((user) => {
    if (user.age == age) {
      data.push({
        name: user.name,
        email: user.email,
      });
    }
  });

  return data;
}

// Implement a loop to access and log the city and country of each individual in the dataset.

function getCitiesAndCountries(users) {
  let data = [];

  users.forEach((user) => {
    data.push({
      city: user.city,
      country: user.country,
    });
  });

  return data;
}

// console.log(getEmails(arrayOfObjects));
// console.log(getHobbiesByAge(arrayOfObjects, 30));
// console.log(getStudentsByCountry(arrayOfObjects, "Australia"));
// console.log(getUserByIndex(arrayOfObjects, 3));
// console.log(getAges(arrayOfObjects));
// console.log(getFirstHobbies(arrayOfObjects));
// console.log(getNamesAndEmailsByAge(arrayOfObjects, 25));
console.log(getCitiesAndCountries(arrayOfObjects));
